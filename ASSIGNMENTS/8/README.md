# Assignment 8
**Due May 1<sup>st</sup>**

## Task
Start with the code handout implement a "World Clock" iOS app (using
Cordova: https://cordova.apache.org/

## Grading
1. 10 pts for packaging app (Cordova).
2. 5 pts for making user selection of time-zones persistant
   (localStorage).
3. 5 pts for Design (CSS)


## Submission
Code submitted on homework git repo.
Demo on Finals day.
