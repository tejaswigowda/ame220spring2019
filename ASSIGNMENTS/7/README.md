# Assignment 7
**Due April 16<sup>th</sup> before midnight**

## Task
Start with the code handout make the app responsive (only designed for "big" screens -- make it work for "small" screens).

## Reference
Refer to the responsive design techniques discussed in class. See `clockApp-responsive` code.

## Submission
Code submitted on homework git repo.
